
import ComboBox from './combobox.js'
/* В файле combobox.js реализуйте ComboBox
* а сюда импортируйте его, например так import {ComboBox} from './combobox.js'

/* Реализуйте функцю поиска городов */
 async function getCityList(url) {
    const response = await fetch(url).then((response) => {
        if (response.ok) {
            return Promise.resolve(response);
        } else return Promise.reject('error');
    }).then(response => response.json());
    return response;
}

/* Реализуйте функцю поиска IDE */
async function getIdeList(url) {
    const response = await fetch(url).then((response) => {
        if (response.ok) {
            return Promise.resolve(response);
        } else return Promise.reject('error');
    }).then(response => response.json());
    return response;
}

/* Добавьте ComboBox для строки "Город"
* например так:
* const city = new ComboBox('city', 'Введите или выберите из списка', getCityList)
* document.getElementById('row_city').appendChild(city.render())
* Ваша реализация может сильно отличаться
* */
const city = new ComboBox('city', 'Введите или выберите из списка', getCityList);
const cityInput = document.getElementById('row_city').children[1].children[0];

cityInput.addEventListener('focus', (event) => {
    city.openList();
    city.render('/city');
    cityInput.addEventListener('input',city.sorted());
});
cityInput.addEventListener('blur', (event) => {
    city.hideList();
});


/* Добавьте ComboBox для строки "Предпочитаемая IDE"*/

const ide = new ComboBox('ide', 'IDE', getIdeList);
const ideInput = document.getElementById('row_ide').children[1].children[0];

ideInput.addEventListener('focus', (event) => {
    ide.openList();
    ide.render('/ide');
    ideInput.addEventListener('input',ide.sorted());
});
ideInput.addEventListener('blur', (event) => {
    ide.hideList();
});
