function debounce(func, time) {
  let iTimeout = null
  return (...args) => {
    if(iTimeout) {
      window.clearTimeout(iTimeout)
    }
    iTimeout = window.setTimeout(() => {
      func(...args)
      iTimeout = null
    }, time)
  }
}

/* Реализуйте ComboBox и экспортируйте его при помощи export {ComboBox}*/

class ComboBox {
  constructor (type, placeholder, func){
    let field;
    this.type = type;
    this.placeholder = placeholder;
    this.func = func;

    if (type === 'city'){
      field = document.getElementById('row_city');
    }
    else  if (type === 'ide'){
      field = document.getElementById('row_ide');
    }

    const combobox = document.createElement('div');
    combobox.classList.add('combobox');
    this.input = document.createElement('input');
    this.input.name = type;
    this.input.placeholder = placeholder;
    this.list = document.createElement('div');
    this.list.classList.add('list');
    this.items = document.createElement('div');
    this.items.classList.add('items-wrapper');
    this.items.style.display = 'none';

    field.appendChild(combobox);
    combobox.append(this.input, this.list);
    this.list.append(this.items);

    this.addListeners();

  }

  clearList() {
    this.items.querySelectorAll('.item').forEach(item => {
      item.remove();
    })
  }


  hideList() {
    this.items.style.display = 'none';
  }

  openList() {
    this.items.style.display = 'block';
  }

  sorted() {
    return debounce(() => {
      this.render(`/${this.type}?name_like=` + this.input.value);
    }, 1000);
  }

  addListeners() {
    this.list.addEventListener('mousedown', (event) => {
      const {target} = event;
      this.input.value = target.innerHTML;
      this.clearList();
    });
    this.list.addEventListener('mouseover', (event) => {
      const {target} = event;
      target.classList.toggle('selected');
    });
    this.list.addEventListener('mouseout', (event) => {
      const {target} = event;
      target.classList.toggle('selected');
    });
    let current;
    this.input.addEventListener('keydown', (event) => {
      if (event.keyCode === 38 || event.keyCode === 40) {
        if (!(this.items.querySelectorAll('.selected').length)) {
          this.items.children[0].classList.toggle('selected');
          return;
        }
        if (event.keyCode === 38) {
          current = this.items.querySelector('.selected');
          if (!(current.previousElementSibling)) {
            current = this.items.children[this.items.children.length - 1];
            current.classList.toggle('selected');
            this.items.children[0].classList.toggle('selected');
          } else {
            current = current.previousElementSibling;
            current.classList.toggle('selected');
            current.nextElementSibling.classList.toggle('selected');
          }
        } else if (event.keyCode === 40) {
          current = this.items.querySelector('.selected');
          if (!(current.nextElementSibling)) {
            current = this.items.children[0];
            current.classList.toggle('selected');
            this.items.children[this.items.children.length - 1].classList.toggle('selected');
          } else {
            current = current.nextElementSibling;
            current.classList.toggle('selected');
            current.previousElementSibling.classList.toggle('selected');
          }
        }
      }
      if(event.keyCode === 13){
        if(this.items.querySelector('.selected')){
          this.input.value = this.items.querySelector('.selected').innerHTML;
        }
      }
    })
  }

  render(url) {
    this.clearList();
    const loadElem = document.createElement('div');
    loadElem.classList.add('item');
    loadElem.innerText = 'Загрузка..';
    this.items.appendChild(loadElem);

    this.func(url).then((response) => {
      this.clearList();

      return Promise.resolve(response);
    }).then((response) => {
      if (response.length === 0) {
        const notFound = document.createElement('div');
        notFound.classList.add('item', 'disabled');
        notFound.innerText = 'Не найдено';

        this.items.appendChild(notFound);
      } else {
        for (let i = 0; i < response.length; i++) {
          const itemsElem = document.createElement('div');
          itemsElem.classList.add('item');
          itemsElem.innerText = response[i].name;
          this.items.appendChild(itemsElem);
          if (response[i].name === this.input.value)
            itemsElem.classList.add('selected');
        }
      }
    })
  }
}

export default ComboBox